/*
 * Meta Class-based Object System
 * Copyright (c) 2001 Timo Savola
 *
 * $Date: 2003-08-28 15:40:26 $
 */

#ifndef CLASS_H
#define CLASS_H

#include <glib.h>
#include <gmodule.h>

typedef struct Class_s              Class;
typedef struct Object_s             Object;
typedef struct Configurable_s       Configurable;

#define EXTEND(t)                   struct t##_s super
#define SUPER(p)                    (&(p)->super)

#define SETTER_SYMBOL_PREFIX_S      "set_"

#define MODULE_GET_CLASS_SYMBOL     _module_get_class
#define MODULE_GET_CLASS_SYMBOL_S   "_module_get_class"
#define MODULE_GET_CLASSES_SYMBOL   _module_get_classes
#define MODULE_GET_CLASSES_SYMBOL_S "_module_get_classes"

#ifdef G_DISABLE_ASSERT
# define assertf(expr, format, args...)
#else
# define assertf(expr, format, args...) \
    G_STMT_START{ if (!(expr)) g_error(format, ##args); }G_STMT_END
#endif

#ifdef NDEBUG
# define debug(msg)
# define debugf(fmt, args...)
#else
# ifdef __GNUC__
#  define debug(msg) \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s:%d %s: " msg, \
          __FILE__, __LINE__, __PRETTY_FUNCTION__)
#  define debugf(fmt, args...) \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s:%d %s: " fmt, \
          __FILE__, __LINE__, __PRETTY_FUNCTION__, ##args)
# else
#  define debug(msg) \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s:%d: " msg, \
          __FILE__, __LINE__)
#  define debugf(fmt, args...) \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s:%d: " fmt, \
          __FILE__, __LINE__, ##args)
# endif
#endif

/*
 * Class
 */

struct Class_s {
    Class *super;
    gboolean (*constructor)(Object *, gpointer);
    void (*destructor)(Object *);
    GHashTable *method_table;
    const char *name;
    Object *proto;
    gsize proto_size;
    guint32 proto_method_offset;
};

extern Class *class_create(const char *, gsize, Class *);
extern void class_set_constructor(Class *, gpointer);
extern void class_set_destructor(Class *, gpointer);
extern void class_set_method(Class *, const char *, gpointer);
extern Class *class_get_super(Class *);
extern const char *class_get_name(Class *);
extern Object *class_get_prototype(Class *);
extern gboolean class_child_of(Class *, Class *);

#define class_new(t, s) \
    class_create(g_string(t), sizeof (struct t##_s), s)

#define class_set_setter_method(p, a,b) \
    class_set_method(p, SETTER_SYMBOL_PREFIX_S a, b)

/*
 * Object
 */

struct Object_s {
    Class *class;
};

extern Object *object_create(Class *, gpointer);
extern void object_destroy(Object *);
extern Class *object_get_class(Object *);
extern gpointer object_get_function(Object *, const char *);

#define object_get_super_function(p, a) \
    object_get_function(class_get_prototype(class_get_super( \
        object_get_class((Object *) p))), a)

#define object_instance_of(p, a) \
    class_child_of(object_get_class((Object *) (p)), a)

/*
 * Configurable
 */

struct Configurable_s {
    EXTEND(Object);
    GPtrArray *(*get_description)(Configurable *);
    gboolean (*is_initialized)(Configurable *);
    gboolean (*set)(Configurable *, const char *, gpointer *);
};

extern Class *configurable_get_class(void);

#define configurable_destroy(p)         object_destroy((Object *) (p))
#define configurable_get_description(p) ((p)->get_description(p))
#define configurable_set(p, a,b)        ((p)->set(p, a,b))
#define configurable_is_initialized(p)  ((p)->is_initialized(p))

/*
 * Module functions
 */

extern void add_module_path(const char *, gboolean);
extern void remove_class(Class *);
extern Class *get_class(const char *);
extern GPtrArray *get_classes(Class *);

#endif
