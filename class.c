/*
 * Meta Class-based Object System
 * Copyright (c) 2001 Timo Savola
 *
 * $Date: 2003-08-28 15:40:26 $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "class.h"

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#define OBJECT_FUNC_PTR(obj, offs) \
    (*(gpointer *) ((gpointer) (obj) + (offs)))

#define INSERT_CLASS(p, c) \
    g_hash_table_insert(p, (gpointer) class_get_name(c), c)

typedef gboolean (SetterFunc)(Configurable *,gpointer *);

const char *class_log_domain = "Class";

/*
 * Class
 */

Class *class_create(const gchar *name,
                    gsize size,
                    Class *super)
{
    Class *this = g_new(struct Class_s, 1);

    this->super = super;
    this->constructor = NULL;
    this->destructor = NULL;
    this->method_table = g_hash_table_new(g_str_hash, g_str_equal);
    this->name = name;
    this->proto = g_malloc0(size);
    this->proto_size = size;
    this->proto_method_offset = sizeof (struct Object_s);

    if (super != NULL) {
        memcpy(this->proto, super->proto, super->proto_size);
        this->proto_method_offset = super->proto_size;
    }
    this->proto->class = this;

    return this;
}

void class_set_constructor(Class *this,
                           gpointer func)
{
    this->constructor = (gboolean (*)(Object*,gpointer)) func;
}

void class_set_destructor(Class *this,
                          gpointer func)
{
    this->destructor = (void (*)(Object *)) func;
}

static guint32 get_method_offset(Class *class,
                                 const gchar *name)
{
    while (class != NULL) {
        gpointer p = g_hash_table_lookup(class->method_table, (gpointer) name);
        if (p != NULL) {
            return GPOINTER_TO_UINT(p);
        }

        class = class->super;
    }

    return 0;
}

void class_set_method(Class *this,
                      const char *name,
                      gpointer func)
{
    guint32 offset = get_method_offset(this, name);

    if (offset == 0) {
        offset = this->proto_method_offset;
        this->proto_method_offset += sizeof (gpointer);

        assertf(this->proto_method_offset <= this->proto_size,
                "too many methods for class \"%s\"; "
                "tried to set method \"%s\"",
                class_get_name(this), name);

        g_hash_table_insert(this->method_table,
                            (gpointer) name,
                            GUINT_TO_POINTER(offset));
	}

    OBJECT_FUNC_PTR(this->proto, offset) = func;
}

const gchar *class_get_name(Class *this)
{
    return this->name;
}

Class *class_get_super(Class *this)
{
    return this->super;
}

gboolean class_child_of(Class *this,
                        Class *super)
{
    Class *c;

    for (c = this; c != NULL; c = c->super) {
        if (c == super) {
            return TRUE;
        }
    }

    return FALSE;
}

Object *class_get_prototype(Class *this)
{
    return this->proto;
}

/*
 * Object
 */

static gboolean construct(Object *object,
                          Class *class,
                          gpointer args)
{
    if (class->super != NULL) {
        if (construct(object, class->super, args) == FALSE) {
            return FALSE;
        }
    }

    if (class->constructor != NULL) {
        return class->constructor(object, args);
    }

    return TRUE;
}

Object *object_create(Class *class,
                      gpointer args)
{
    Object *this = (Object *) g_memdup(class->proto, class->proto_size);

    if (construct(this, class, args) == FALSE) {
        g_free(this);
        this = NULL;
    }

    return this;
}

void object_destroy(Object *this)
{
    Class *c;

    for (c = this->class; c != NULL; c = c->super) {
        if (c->destructor != NULL) {
            c->destructor(this);
        }
    }

    g_free(this);
}

Class *object_get_class(Object *this)
{
    return this->class;
}

gpointer object_get_function(Object *this,
                             const gchar *name)
{
    guint32 offset = get_method_offset(this->class, name);
    gpointer func = NULL;
	
    if (offset != 0) {
        func = OBJECT_FUNC_PTR(this, offset);
    }

    return func;
}

/*
 * Configurable
 */

static GPtrArray *_cfg_get_description(Configurable *this)
{
    return g_ptr_array_new();
}

static gboolean _cfg_is_initialized(Configurable *this)
{
    return TRUE;
}

static gboolean _cfg_set(Configurable *this,
                         const gchar *name,
                         gpointer *arg)
{
    char *fname;
    SetterFunc *func = NULL;

    fname = g_strconcat(SETTER_SYMBOL_PREFIX_S, name, NULL);

    func = (SetterFunc *) object_get_function((Object *) this, fname);
    if (func == NULL) {
        g_warning("method '%s' not found in class '%s'",
                  fname, class_get_name(object_get_class((Object *) this)));

        g_free(fname);
        return FALSE;
    }

    g_free(fname);

    return func(this, arg);
}

Class *configurable_get_class(void)
{
    static Class *c = NULL;
    if (c == NULL) {
        c = class_new(Configurable, NULL);
        class_set_method(c, "get_description", _cfg_get_description);
        class_set_method(c, "is_initialized",  _cfg_is_initialized);
        class_set_method(c, "set",             _cfg_set);
    }
    return c;
}

/*
 * Module functions
 */

static GHashTable *classes = NULL;

static void add_class(Class *class)
{
    char *name = (char *) class_get_name(class);
    if (g_hash_table_lookup(classes, name) == NULL) {
        g_hash_table_insert(classes, name, class);
    } else {
        g_warning("found multiple classes with name '%s'", name);
    }
}

static void add_module(const char *path)
{
    GModule *mod;
    gboolean has_class, has_classes;
    Class *(*get_class)(void);
    Class **(*get_classes)(void);

    mod = g_module_open(path, 0);
    if (mod == NULL) {
        g_warning(g_module_error());
        return;
    }

    has_class = g_module_symbol(mod, MODULE_GET_CLASS_SYMBOL_S,
                                (gpointer *) &get_class);
    has_classes = g_module_symbol(mod, MODULE_GET_CLASSES_SYMBOL_S,
                                  (gpointer *) &get_classes);
    if (!has_class && !has_classes) {
        g_warning("unable to find '"MODULE_GET_CLASS_SYMBOL_S"' or '"
                  MODULE_GET_CLASSES_SYMBOL_S"' function in module \"%s\"",
                  path);
        g_module_close(mod);
        return;
    }

    g_module_make_resident(mod);

    if (has_class) {
        Class *class = get_class();
        if (class != NULL) {
            add_class(class);
        } else {
            g_warning("unable to get class from module \"%s\"", path);
        }
    }

    if (has_classes) {
        Class **array = get_classes();
        if (array != NULL) {
            for (; *array != NULL; array++) {
                add_class(*array);
            }
        } else {
            g_warning("unable to get classes from module \"%s\"", path);
        }
    }
}

static void add_path(const char *path,
                     gboolean recursive,
                     gboolean first)
{
    DIR *dir = opendir(path);

    if (dir == NULL) {
        if (errno != ENOTDIR) {
            const char *r;
            switch (errno) {
		   	    case EACCES: r = "permission denied";         break;
                case EMFILE: r = "too many descriptors open"; break;
                case ENOENT: r = "file doesn't exist";        break;
                case ENOMEM: r = "out of memory";             break;
                default:     r = "?";
            }
            g_warning("unable to open directory \"%s\"; %s", path, r);
        }

        add_module(path);
        return;
    }

    if (recursive || first) {
        while (TRUE) {
            struct dirent *entry;
            char *name;

            entry = readdir(dir);
            if (entry == NULL) {
                break;
            }

            if (strncmp(entry->d_name, ".", 1) == 0) {
                continue;
            }

            name = g_strconcat(path, G_DIR_SEPARATOR_S, entry->d_name, NULL);
            add_path(name, recursive, FALSE);
            g_free(name);
        }
    }

    closedir(dir);
}

void add_module_path(const char *path,
                     gboolean recursive)
{
    if (classes == NULL) {
        classes = g_hash_table_new(g_str_hash, g_str_equal);
    }

    add_path(path, recursive, TRUE);
}

Class *get_class(const char *name)
{
    if (classes != NULL) {
        return g_hash_table_lookup(classes, (gpointer) name);
    } else {
        return NULL;
    }
}

static void add_class_maybe(gpointer key,
                            gpointer value,
                            gpointer *ptrs)
{
    if (value != NULL) {
        Class *class = (Class *) value;
        Class *super = (Class *) ptrs[1];
        GPtrArray *array = (GPtrArray *) ptrs[0];

        if (super == NULL || class_child_of(class, super)) {
            g_ptr_array_add(array, class);
        }
    }
}

GPtrArray *get_classes(Class *class)
{
    gpointer ptrs[2];
    GPtrArray *array;

    array = g_ptr_array_new();

    ptrs[0] = array;
    ptrs[1] = class;
    g_hash_table_foreach(classes, (GHFunc) add_class_maybe, (gpointer) &ptrs);

    return array;
}
