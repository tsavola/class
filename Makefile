# $Date: 2003-08-28 15:40:26 $

TARGET= libclass.so

OBJS  =	class.o

CC     = gcc
CFLAGS = -ansi -Wall -Wmissing-prototypes -DG_LOG_DOMAIN=class_log_domain \
	 -g `glib-config --cflags glib gmodule` -fPIC
LDFLAGS= -shared `glib-config --libs glib gmodule`

$(TARGET): $(OBJS) Makefile
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $*.c

clean:
	rm -f $(TARGET) $(OBJS) *~

class.o: class.c class.h
